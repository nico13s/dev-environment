#!/bin/bash

BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'

unset DYLD_LIBRARY_PATH
unset LD_LIBRARY_PATH

DOCKER_MACHINE=/usr/local/bin/docker-machine
VM=vmfusion

RESULT=$("${DOCKER_MACHINE}" ls | grep "${VM}" | grep Running | wc -l)
if [ $RESULT -eq 0 ]; then
  "${DOCKER_MACHINE}" create --driver vmwarefusion --vmwarefusion-cpu-count 4 --vmwarefusion-memory-size 4096 "${VM}"
# else
#   echo 'outside'
fi

VM_STATUS="$(${DOCKER_MACHINE} status ${VM} 2>&1)"

if [ "${VM_STATUS}" != "Running" ]; then
  "${DOCKER_MACHINE}" start "${VM}"
  yes | "${DOCKER_MACHINE}" regenerate-certs "${VM}"
fi

eval "$(${DOCKER_MACHINE} env --shell=bash ${VM})"

clear
cat << EOF


                        ##         .
                  ## ## ##        ==
               ## ## ## ## ##    ===
           /"""""""""""""""""\___/ ===
      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
           \______ o           __/
             \    \         __/
              \____\_______/


EOF
echo -e "${BLUE}docker${NC} is configured to use the ${GREEN}${VM}${NC} machine with IP ${GREEN}$(${DOCKER_MACHINE} ip ${VM})${NC}"
echo "For help getting started, check out the docs at https://docs.docker.com"
echo

RESULT=$(netstat -rn | grep "172.17" | wc -l)
if [ $RESULT -ne 0 ]; then
  sudo route -n delete -net 172.17.0.0/16
fi
# sudo route -n delete -net 172.17.0.0/16 $(${DOCKER_MACHINE} ip ${VM})
sudo route -n add -net 172.17.0.0/16 $(${DOCKER_MACHINE} ip ${VM})

USER_SHELL="$(dscl /Search -read /Users/${USER} UserShell | awk '{print $2}' | head -n 1)"
if [[ "${USER_SHELL}" == *"/bash"* ]] || [[ "${USER_SHELL}" == *"/zsh"* ]] || [[ "${USER_SHELL}" == *"/sh"* ]]; then
  "${USER_SHELL}" --login
else
  "${USER_SHELL}"
fi