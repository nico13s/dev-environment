Projet pour construire un environnement de développement autour de PHP et des grands classiques: MySQL, Memcache ...


Pour lancer l'environnement, il suffit de faire un "docker-compose up -d".

Si vous etes sur windows ou mac, il faut d'abord initier la vm.
Sur OSX, avec Fusion, il faut lancer le script docker_fusion_start qui va construire la vm si elle n'existe pas. Démarrer la vm si elle n'est pas démarrée. Et changer les variables d'environnement pour être prêt à utiliser l'environnement.

Important: pensez à changer votre dns pour utiliser celui de l'environnement. Mettre 172.17.0.1.

Une fois l'environnement lancé, vous devriez avoir accès à http://phpinfo.docker ou http://phpmyadmin.docker/.

Pour créer un nouveau projet: 
* Créer le dossier du projet dans le dossier projects
* Modifier la configuration du container web et ajouter l'url souhaitée dans l'alias
* Configurer nginx: config/shared/nginx